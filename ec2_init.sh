#!/bin/bash

sudo yum update -y
sudo yum install -y nginx
sudo sed -i 's/80 default_server;/8080 default_server;/g' /etc/nginx/nginx.conf
sudo /bin/cp configs/static_s3_bucket.conf /etc/nginx/default.d/
sudo /bin/cp www_content/* /usr/share/nginx/html
sudo service nginx restart
